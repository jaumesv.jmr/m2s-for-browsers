var keyuser = localStorage.getItem('keyuser');
$('.li-chats.settings').click(function(){
	var id = $(this).attr('id');
	$('.li-chats.settings').removeClass('active');
	$(this).addClass('active');
	var textmods = $('.li-chats.settings.active .right-p .name').html();
	$('.center-align').hide();
    $('.modsettings').hide();
    $('.modsettings#'+id).show();
    if ($(window).width() <= '500') {
    	$('.chat-messages').show();
    	$('.chat-messages').css('padding-bottom','0px');
    	$('.nav.navbar-nav li').css('display', 'none');
    	$('#menu.nav.navbar-nav').append('<div id="forw"><span class="icon chevron-left"></span><div class="center">' + textmods + '</div></div>');
        $('#forw .icon.chevron-left').click(function() { 
        	$('.chat-messages').hide();
        	$('.nav.navbar-nav li').css('display', 'block');
        	$('#forw').remove();
        	$('.li-chats.settings').removeClass('active');
        	$('.center-align').show();
        	$('.modsettings').hide();
        })
    }
});

if ($(window).width() > '991') {
    $('.modsettings').css('width','500px');
    $('.modsettings').css('position','relative');
    $('.modsettings').css('left','50%');
    $('.modsettings').css('margin-left','-250px');
}

$(window).resize(function() {
  if ($(window).width() > '500') {
  	 $('.chat-messages').removeAttr('style');
  	 $('#forw').remove();
  	 $('.nav.navbar-nav li').css('display', 'block');
  }
  if ($(window).width() <= '500') {
     $('.li-chats.settings').removeClass('active');
     $('.center-align').show();
     $('.modsettings').hide();
  }
  if ($(window).width() <= '991') {
     $('.modsettings').removeAttr('style');
  }
  if ($(window).width() > '991') {
    $('.modsettings').css('width','500px');
    $('.modsettings').css('position','relative');
    $('.modsettings').css('left','50%');
    $('.modsettings').css('margin-left','-250px');
}
})

$('.profileimg').attr("src","http://www.gravatar.com/avatar.php?gravatar_id="+localStorage.emailMD5+"&rating=G&size=150");

$('#updtphbtn').click(function(){
	var newphnum = $('#mophn').val();
	$.ajax({
      type: "GET",
      crossDomain: true,
      url: "http://m2s.es/app/api/connect/editprofile.php",
      data: "phonenumber=" + newphnum + "&key=" + keyuser,
      cache: false,
      dataType: 'jsonp',
      success: function(result) {
          console.log(result.mensaje);
          if(result.mensaje == 'ok'){
          	infomod(Language.updatephonesucces);
          	$('#mophn').val('');
          }
      }
     })
})

$('#updtpassw').click(function(){
	var newpassw = $('#newpswin').val();
	var confpassw = $('#newpswin_conf').val();
	$.ajax({
      type: "GET",
      crossDomain: true,
      url: "http://m2s.es/app/api/connect/editprofile.php",
      data: "password=" + newphnum + "&password_conf=" + confpassw + "&key=" + keyuser,
      cache: false,
      dataType: 'jsonp',
      success: function(result) {
          console.log(result.mensaje);
          if(result.mensaje == 'ok'){
          	infomod(Language.updatepasssucces);
          	$('#newpswin').val('');
          	$('#newpswin_conf').val('');
          	localStorage.removeItem('keyuser');
          }
      }
     })
})