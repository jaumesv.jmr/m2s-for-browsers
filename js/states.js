var keyuser = localStorage.getItem('keyuser');
var divs = '';

$('#groupbsu').click(function() {
  if ($('#groupbsu.active').length == '0') {
    $('#groupbsu').addClass('active');
    $('#friendbsu').removeClass('active');
    $('#timeline.friends-timeline').hide();
    $('#timeline.groups-timeline').show();
    timelinegroupsinf();
  }
});

$('#friendbsu').click(function() {
  if ($('#friendbsu.active').length == '0') {
    $('#friendbsu').addClass('active');
    $('#groupbsu').removeClass('active');
    $('#timeline.groups-timeline').hide();
    $('#timeline.friends-timeline').show();
    timelinefriendsinf();
  }
});

function timelinefriendsinf() {
  $.ajax({
    type: "GET",
    jsonpCallback: 'jpCallback',
    crossDomain: true,
    url: "http://m2s.es/app/api/states.php?s=friends&key=" + keyuser,
    cache: false,
    dataType: 'jsonp',
    beforeSend: function() {
      console.log("Loading states...");
      if ($('#timeline.friends-timeline .item').length == '0' && $('#timeline.friends-timeline h3#nostates').length == '0') {
        $('#timeline.friends-timeline').append('<div id="loading-user"><img src="css/loading.gif" width="25px" height="25px"/> <span>Loading...</span></div>');
      }
    },
    success: function(result) {
      $('#timeline.friends-timeline #loading-user').remove();
      if (result.states) {
        if (result.states.length == '0') {
          console.log('No states!');
          if ($('#timeline.friends-timeline h3#nostates').length == '0') {
            $('#timeline.friends-timeline').append('<h3 id="nostates">' + Language.nostatesfriends + '</h3>');
          }
        }
        for (var i = 0; i < result.states.length; i++) {
          id = result.states[i].id;
          username = result.states[i].username;
          imagepr = result.states[i].imagepr;
          message = result.states[i].message;
          date = result.states[i].date;
          message = linkscom(message);
          if ($('#timeline.friends-timeline .item#' + id).length == '0') {
            addtotimeline(id, imagepr, username, date, message);
          }
        };
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.error(textStatus + ' ' + XMLHttpRequest.status);
      var divno = document.createElement('div');
      divno.innerHTML = '<div id="list-notifications" style="text-align:center;cursor:pointer"></div>';
      divno.className = 'background-dark';
      var modsv = document.getElementById('modsv');
      modsv.appendChild(divno);
      $('#list-notifications').append('<b>' + Language.error + ':</b> ' + Language.errorconection);
      $('#list-notifications').click(function() {
        window.location.reload();
      })
    }
  });
}

timelinefriendsinf();

$$('.friends-timeline').swipeDown(function() {
  console.log('Refreshing...');
  $('#timeline.friends-timeline').prepend('<div class="refresh"><img src="css/loading.gif" width="25px" height="25px"/><span> ' + Language.loading + '</span></div>');
  timelinefriendsinf();
  $('#timeline.friends-timeline .refresh').remove();
  console.log('Refreshed!');
});

function timelinegroupsinf() {
  $.ajax({
    type: "GET",
    jsonpCallback: 'jpCallback',
    crossDomain: true,
    url: "http://m2s.es/app/api/states.php?s=groups&key=" + keyuser,
    cache: false,
    dataType: 'jsonp',
    beforeSend: function() {
      console.log("Loading states...");
      if ($('#timeline.groups-timeline .item').length == '0' && $('#timeline.groups-timeline h3#nostates').length == '0') {
        $('#timeline.groups-timeline').append('<div id="loading-user"><img src="css/loading.gif" width="25px" height="25px"/> <span>Loading...</span></div>');
      }
    },
    success: function(result) {
      $('#timeline.groups-timeline #loading-user').remove();
      if (result.states) {
        if (result.states.length == '0') {
          console.log('No states!');
          if ($('#timeline.groups-timeline h3#nostates').length == '0') {
            $('#timeline.groups-timeline').append('<h3 id="nostates">' + Language.nostatesgroups + '</h3>');
          }
        }
        for (var i = 0; i < result.states.length; i++) {
          id = result.states[i].id;
          username = result.states[i].username;
          imagepr = result.states[i].imagepr;
          message = result.states[i].message;
          date = result.states[i].date;
          message = linkscom(message);
          if ($('#timeline.groups-timeline .item#' + id).length == '0') {
            addtotimelinegr(id, imagepr, username, date, message);
          }
        };
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.error(textStatus + ' ' + XMLHttpRequest.status);
      var divno = document.createElement('div');
      divno.innerHTML = '<div id="list-notifications" style="text-align:center;cursor:pointer"></div>';
      divno.className = 'background-dark';
      var modsv = document.getElementById('modsv');
      modsv.appendChild(divno);
      $('#list-notifications').append('<b>' + Language.error + ':</b> ' + Language.errorconection);
      $('#list-notifications').click(function() {
        window.location.reload();
      })
    }
  });
}

$$('.groups-timeline').swipeDown(function() {
  console.log('Refreshing...');
  $('#timeline.groups-timeline').prepend('<div class="refresh"><img src="css/loading.gif" width="25px" height="25px"/><span> ' + Language.loading + '</span></div>');
  timelinegroupsinf();
  $('#timeline.groups-timeline .refresh').remove();
  console.log('Refreshed!');
});

$('#state-post').click(function() {
  var writetext = $('#share-state textarea').val();
  $.ajax({
    type: "GET",
    jsonpCallback: 'jpCallback',
    crossDomain: true,
    url: "http://m2s.es/app/api/connect/write-state.php?key=" + keyuser + "&txt=" + writetext,
    cache: false,
    dataType: 'jsonp',
    beforeSend: function() {
      $('#state-post').attr('disabled');
      $('#state-post').html(Language.loading);
    },
    success: function(writers) {
      if (writers.mensaje == 'ok') {
        $('#share-state').modal('hide');
        timelinefriendsinf();
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.error(textStatus + ' ' + XMLHttpRequest.status);
      errormod(Language.errorpetitionm2s);
      $('#state-post').removeAttr("disabled");
      $('#state-post').html(Language.post);
    }
  })
});

$('#file-input').change(function(e) {
  var file = e.target.files[0],
    imageType = /image.*/;

  if (!file.type.match(imageType))
    return;

  var reader = new FileReader();
  reader.onload = fileOnload;
  reader.readAsDataURL(file);

});

function fileOnload(e) {
  var $img = $('<img>', {
    src: e.target.result
  });
  var canvas = $('#canvas')[0];
  $('#file-input').val('');
  $img.load(function() {
    canvas.width = this.width;
    canvas.height = this.height;
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(this, 0, 0, this.width, this.height);
  });
  $('#more-share').modal('hide');
  $('#img-mod').modal('show');
}
$('#upload-image').click(function() {
  $('#upload-image').attr('disabled', 'disabled');
  var canvas = document.getElementById("canvas");
  var url = canvas.toDataURL("image/png", 1.0);
  url = url.replace('data:image/png;base64,', '');
  console.log(url);
  $.ajax({
    url: 'https://api.imgur.com/3/image',
    method: 'POST',
    headers: {
      Authorization: 'Client-ID def4c03828b22c2',
      Accept: 'application/json'
    },
    data: {
      image: url,
      type: 'base64'
    },
    success: function(result) {
      var link = result.data.link;
      $('#share-state textarea').val($('#share-state textarea').val() + ' ' + link);
      $('#upload-image').removeAttr("disabled");
      $('#img-mod').modal('hide');
    }
  });
});